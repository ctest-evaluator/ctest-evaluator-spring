package de.fopro.ctest.models;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "ctests")
public class CTest {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "created_on")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn = new Date();

	@NotBlank
	@Size(max = 255)
	private String name;
	
	private int nrOfGaps;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "language_id", nullable = false)
	private Language language;

	@NotBlank
	@Size(max = 50)
	private String type;

	private int estimatedDifficulty;

	private Float calculatedDifficulty;

	private Float evaluatedDifficulty;
	
	@OneToMany(mappedBy = "ctest", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonManagedReference("Ctest-Token")
	@OrderBy("id ASC")
	private Set<CTestToken> cTestTokens;

	@OneToMany(mappedBy = "ctest", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonManagedReference("Ctest-Evaluation")
	private Set<CTestEvaluation> cTestEvaluations;

	public CTest() {
	}

	public CTest(String name, Language language, String type, int nrOfGaps, int estimatedDifficulty, Float calculatedDifficulty,
			Float evaluatedDifficulty) {
		this.name = name;
		this.language = language;
		this.type = type;
		this.nrOfGaps = nrOfGaps;
		this.estimatedDifficulty = estimatedDifficulty;
		this.calculatedDifficulty = calculatedDifficulty;
		this.evaluatedDifficulty = evaluatedDifficulty;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNrOfGaps() {
		return nrOfGaps;
	}

	public void setNrOfGaps(int nrOfGaps) {
		this.nrOfGaps = nrOfGaps;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getEstimatedDifficulty() {
		return estimatedDifficulty;
	}

	public void setEstimatedDifficulty(int estimatedDifficulty) {
		this.estimatedDifficulty = estimatedDifficulty;
	}

	public Float getCalculatedDifficulty() {
		return calculatedDifficulty;
	}

	public void setCalculatedDifficulty(Float calculatedDifficulty) {
		this.calculatedDifficulty = calculatedDifficulty;
	}

	public Float getEvaluatedDifficulty() {
		return evaluatedDifficulty;
	}

	public void setEvaluatedDifficulty(Float evaluatedDifficulty) {
		this.evaluatedDifficulty = evaluatedDifficulty;
	}

	public Set<CTestToken> getcTestTokens() {
		return cTestTokens;
	}

	public void setcTestTokens(Set<CTestToken> cTestTokens) {
		this.cTestTokens = cTestTokens;
	}

	public Set<CTestEvaluation> getcTestEvaluations() {
		return cTestEvaluations;
	}

	public void setcTestEvaluations(Set<CTestEvaluation> cTestEvaluations) {
		this.cTestEvaluations = cTestEvaluations;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		CTest cTest = (CTest) o;

		return id != null ? id.equals(cTest.id) : cTest.id == null;
	}

	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}

	@Override
	public String toString() {
		return "CTest{" +
				"id=" + id +
				", createdOn=" + createdOn +
				", name='" + name + '\'' +
				", nrOfGaps=" + nrOfGaps +
				", language=" + language +
				", type='" + type + '\'' +
				", estimatedDifficulty=" + estimatedDifficulty +
				", calculatedDifficulty=" + calculatedDifficulty +
				", evaluatedDifficulty=" + evaluatedDifficulty +
				'}';
	}
}
