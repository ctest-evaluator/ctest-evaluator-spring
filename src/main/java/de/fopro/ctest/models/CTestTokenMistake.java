package de.fopro.ctest.models;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.*;

@Entity
@Table(name = "ctest_token_mistakes")
public class CTestTokenMistake {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
    @ManyToOne(fetch = FetchType.LAZY, optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "ctest_token_id", nullable = false)
	@JsonBackReference("Ctest-Token-Mistake")
    private CTestToken ctestToken;

    private String value;
    
    private Integer count;
	
    public CTestTokenMistake() {}

	public CTestTokenMistake(CTestToken ctestToken, String value, Integer count) {
		super();
		this.ctestToken = ctestToken;
		this.value = value;
		this.count = count;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CTestToken getCtestToken() {
		return ctestToken;
	}

	public void setCtestToken(CTestToken ctestToken) {
		this.ctestToken = ctestToken;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	};

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		CTestTokenMistake that = (CTestTokenMistake) o;

		return id != null ? id.equals(that.id) : that.id == null;
	}

	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}

	@Override
	public String toString() {
		return "CTestTokenMistake{" +
				"id=" + id +
				", value='" + value + '\'' +
				", count=" + count +
				'}';
	}
}
