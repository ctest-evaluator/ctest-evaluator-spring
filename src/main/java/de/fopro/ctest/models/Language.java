package de.fopro.ctest.models;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "language", uniqueConstraints = { @UniqueConstraint(columnNames = "languageCode") })
public class Language {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank
	@Size(min=1, max = 3)
	private String languageCode;
	
	@NotBlank
	@Size(max = 255)
	private String language;

	@OneToMany(mappedBy = "language", fetch = FetchType.LAZY)
	@JsonIgnore
	private Set<CTest> ctests;

	public Language() {}

	public Language(String language, String languageCode) {
		this.languageCode = languageCode;
		this.language = language;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
	public Set<CTest> getCtests() {
		return ctests;
	}

	public void setCtests(Set<CTest> ctests) {
		this.ctests = ctests;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Language language = (Language) o;

		return id != null ? id.equals(language.id) : language.id == null;
	}

	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}

	@Override
	public String toString() {
		return "Language{" +
				"id=" + id +
				", languageCode='" + languageCode + '\'' +
				", language='" + language + '\'' +
				'}';
	}
}
