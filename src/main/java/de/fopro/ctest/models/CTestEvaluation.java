package de.fopro.ctest.models;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ctest_evaluations")
public class CTestEvaluation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn = new Date();

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "ctest_id", nullable = false)
    @JsonBackReference("Ctest-Evaluation")
    private CTest ctest;

    private int numberCorrectGaps;

    private String userTrainedLanguage;

    private Integer userSelfEstimatedLanguageLevel;

    private Integer userAge;

    private String userNativeLanguage;

    public CTestEvaluation () {
    }

    public CTestEvaluation (
            CTest ctest,
            int numberCorrectGaps,
            String userTrainedLanguage,
            Integer userSelfEstimatedLanguageLevel,
            Integer userAge, String userNativeLanguage)
    {
        this.ctest = ctest;
        this.numberCorrectGaps = numberCorrectGaps;
        this.userTrainedLanguage = userTrainedLanguage;
        this.userSelfEstimatedLanguageLevel = userSelfEstimatedLanguageLevel;
        this.userAge = userAge;
        this.userNativeLanguage = userNativeLanguage;
    }

    public String getUserTrainedLanguage () {
        return userTrainedLanguage;
    }

    public void setUserTrainedLanguage (String userTrainedLanguage) {
        this.userTrainedLanguage = userTrainedLanguage;
    }

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public Date getCreatedOn () {
        return createdOn;
    }

    public void setCreatedOn (Date createdOn) {
        this.createdOn = createdOn;
    }

    public CTest getCtest () {
        return ctest;
    }

    public void setCtest (CTest ctest) {
        this.ctest = ctest;
    }

    public int getNumberCorrectGaps () {
        return numberCorrectGaps;
    }

    public void setNumberCorrectGaps (int numberCorrectGaps) {
        this.numberCorrectGaps = numberCorrectGaps;
    }

    public Integer getUserSelfEstimatedLanguageLevel () {
        return userSelfEstimatedLanguageLevel;
    }

    public void setUserSelfEstimatedLanguageLevel (Integer userSelfEstimatedLanguageLevel) {
        this.userSelfEstimatedLanguageLevel = userSelfEstimatedLanguageLevel;
    }

    public Integer getUserAge () {
        return userAge;
    }

    public void setUserAge (Integer userAge) {
        this.userAge = userAge;
    }

    public String getUserNativeLanguage () {
        return userNativeLanguage;
    }

    public void setUserNativeLanguage (String userNativeLanguage) {
        this.userNativeLanguage = userNativeLanguage;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        CTestEvaluation that = (CTestEvaluation) o;

        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode () {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString () {
        return "CTestEvaluation{" +
                "id=" + id +
                ", createdOn=" + createdOn +
                ", numberCorrectGaps=" + numberCorrectGaps +
                ", userTrainedLanguage='" + userTrainedLanguage + '\'' +
                ", userSelfEstimatedLanguageLevel=" + userSelfEstimatedLanguageLevel +
                ", userAge=" + userAge +
                ", userNativeLanguage='" + userNativeLanguage + '\'' +
                '}';
    }
}
