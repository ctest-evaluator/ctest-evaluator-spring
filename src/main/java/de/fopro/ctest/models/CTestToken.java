package de.fopro.ctest.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "ctest_tokens")
public class CTestToken {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "ctest_id", nullable = false)
    @JsonBackReference("Ctest-Token")
    private CTest ctest;
    
    private String value;
    
    private Integer offset;
    
    private Boolean gapStatus;

    private Boolean isNormal;

	private Float calculatedGapDifficulty;
    
	private Float evaluatedGapDifficulty;
	
    private Boolean isLastTokenInSentence;

	@OneToMany(mappedBy = "ctestToken", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonManagedReference("Ctest-Token-Mistake")
	private Set<CTestTokenMistake> cTestTokensMistakes;
	
	public CTestToken() {}

	public CTestToken(CTest ctest, String value, Integer offset, Boolean gapStatus, Boolean isNormal,
			Float calculatedGapDifficulty, Float evaluatedGapDifficulty, Boolean isLastTokenInSentence) {
		super();
		this.ctest = ctest;
		this.value = value;
		this.offset = offset;
		this.gapStatus = gapStatus;
		this.isNormal = isNormal;
		this.calculatedGapDifficulty = calculatedGapDifficulty;
		this.evaluatedGapDifficulty = evaluatedGapDifficulty;
		this.isLastTokenInSentence = isLastTokenInSentence;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CTest getCtest() {
		return ctest;
	}

	public void setCtest(CTest ctest) {
		this.ctest = ctest;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Boolean getGapStatus() {
		return gapStatus;
	}

	public void setGapStatus(Boolean gapStatus) {
		this.gapStatus = gapStatus;
	}

	public Boolean getIsNormal() {
		return isNormal;
	}

	public void setIsNormal(Boolean isNormal) {
		this.isNormal = isNormal;
	}

	public Float getCalculatedGapDifficulty() {
		return calculatedGapDifficulty;
	}

	public void setCalculatedGapDifficulty(Float calculatedGapDifficulty) {
		this.calculatedGapDifficulty = calculatedGapDifficulty;
	}

	public Float getEvaluatedGapDifficulty() {
		return evaluatedGapDifficulty;
	}

	public void setEvaluatedGapDifficulty(Float evaluatedGapDifficulty) {
		this.evaluatedGapDifficulty = evaluatedGapDifficulty;
	}

	public Boolean getIsLastTokenInSentence() {
		return isLastTokenInSentence;
	}

	public void setIsLastTokenInSentence(Boolean isLastTokenInSentence) {
		this.isLastTokenInSentence = isLastTokenInSentence;
	}

	public Set<CTestTokenMistake> getcTestTokensMistakes() {
		return cTestTokensMistakes;
	}

	public void setcTestTokensMistakes(Set<CTestTokenMistake> cTestTokensMistakes) {
		this.cTestTokensMistakes = cTestTokensMistakes;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		CTestToken that = (CTestToken) o;

		return id != null ? id.equals(that.id) : that.id == null;
	}

	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}

	@Override
	public String toString() {
		return "CTestToken{" +
				"id=" + id +
				", value='" + value + '\'' +
				", offset=" + offset +
				", gapStatus=" + gapStatus +
				", isNormal=" + isNormal +
				", calculatedGapDifficulty=" + calculatedGapDifficulty +
				", evaluatedGapDifficulty=" + evaluatedGapDifficulty +
				", isLastTokenInSentence=" + isLastTokenInSentence +
				'}';
	}
}
