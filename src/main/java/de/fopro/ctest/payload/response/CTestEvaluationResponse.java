package de.fopro.ctest.payload.response;

import de.fopro.ctest.payload.response.wrapper.CTestEvaluationTokenResponse;

import java.util.List;

public class CTestEvaluationResponse {

  List<CTestEvaluationTokenResponse> cTestEvaluationTokenResponses;
  private long ctestId;
  private int userEstimatedLanguageLevel;
  private int numberCorrectGaps;
  private boolean isCtestCorrect;
  private float scoring;

  public CTestEvaluationResponse(
      long ctestId,
      List<CTestEvaluationTokenResponse> cTestEvaluationTokenResponses,
      int userEstimatedLanguageLevel,
      int numberCorrectGaps,
      boolean isCtestCorrect,
      float scoring) {
    this.ctestId = ctestId;
    this.cTestEvaluationTokenResponses = cTestEvaluationTokenResponses;
    this.userEstimatedLanguageLevel = userEstimatedLanguageLevel;
    this.numberCorrectGaps = numberCorrectGaps;
    this.isCtestCorrect = isCtestCorrect;
    this.scoring = scoring;
  }

  public long getCtestId() {
    return ctestId;
  }

  public void setCtestId(long ctestId) {
    this.ctestId = ctestId;
  }

  public List<CTestEvaluationTokenResponse> getcTestEvaluationTokenResponses() {
    return cTestEvaluationTokenResponses;
  }

  public void setcTestEvaluationTokenResponses(
      List<CTestEvaluationTokenResponse> cTestEvaluationTokenResponses) {
    this.cTestEvaluationTokenResponses = cTestEvaluationTokenResponses;
  }

  public int getUserEstimatedLanguageLevel() {
    return userEstimatedLanguageLevel;
  }

  public void setUserEstimatedLanguageLevel(int userEstimatedLanguageLevel) {
    this.userEstimatedLanguageLevel = userEstimatedLanguageLevel;
  }

  public int getNumberCorrectGaps() {
    return numberCorrectGaps;
  }

  public void setNumberCorrectGaps(int numberCorrectGaps) {
    this.numberCorrectGaps = numberCorrectGaps;
  }

  public boolean isCtestCorrect() {
    return isCtestCorrect;
  }

  public void setCtestCorrect(boolean ctestCorrect) {
    isCtestCorrect = ctestCorrect;
  }

  public float getScoring() {
    return scoring;
  }

  public void setScoring(float scoring) {
    this.scoring = scoring;
  }
}
