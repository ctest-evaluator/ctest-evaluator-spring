package de.fopro.ctest.payload.response.wrapper;

import de.fopro.ctest.models.CTestToken;

public class CTestEvaluationTokenResponse {

    private CTestToken ctestToken;
    private String userGapValue;
    private boolean isCorrect;

    public CTestEvaluationTokenResponse(CTestToken ctestToken, String userGapValue, boolean isCorrect) {
        this.ctestToken = ctestToken;
        this.userGapValue = userGapValue;
        this.isCorrect = isCorrect;
    }

    public CTestToken getCtestToken() {
        return ctestToken;
    }

    public void setCtestToken(CTestToken ctestToken) {
        this.ctestToken = ctestToken;
    }

    public String getUserGapValue() {
        return userGapValue;
    }

    public void setUserGapValue(String userGapValue) {
        this.userGapValue = userGapValue;
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public void setCorrect(boolean correct) {
        isCorrect = correct;
    }
}
