package de.fopro.ctest.payload.response;

import de.fopro.ctest.models.CTestEvaluation;
import de.fopro.ctest.models.CTestToken;
import de.fopro.ctest.models.Language;

import java.util.Set;

public class CTestResponse {
    private Long id;
    private String name;
    private Integer nrOfGaps;
    private String language;
    private String languageCode;
    private String type;
    private boolean fetchedFromEvaluations;
    private int estimatedDifficulty;
    private Float calculatedDifficulty;
    private Float evaluatedDifficulty;
    private Set<CTestToken> cTestTokens;
    private Set<CTestEvaluation> cTestEvaluations;

    public CTestResponse(Long id, String name, Integer nrOfGaps, Language language, String type,
                         int estimatedDifficulty, Float calculatedDifficulty, Float evaluatedDifficulty,
                         Set<CTestToken> cTestTokens) {
        super();
        this.id = id;
        this.name = name;
        this.nrOfGaps = nrOfGaps;
        this.language = language.getLanguage();
        this.languageCode = language.getLanguageCode();
        this.type = type;
        this.estimatedDifficulty = estimatedDifficulty;
        this.calculatedDifficulty = calculatedDifficulty;
        this.evaluatedDifficulty = evaluatedDifficulty;
        this.cTestTokens = cTestTokens;
    }

    public CTestResponse(Long id, String name, Integer nrOfGaps, Language language, String type,
                         int estimatedDifficulty, Float calculatedDifficulty, Float evaluatedDifficulty,
                         Set<CTestToken> cTestTokens, boolean fetchedFromEvaluations) {
        super();
        this.id = id;
        this.name = name;
        this.nrOfGaps = nrOfGaps;
        this.language = language.getLanguage();
        this.languageCode = language.getLanguageCode();
        this.type = type;
        this.estimatedDifficulty = estimatedDifficulty;
        this.calculatedDifficulty = calculatedDifficulty;
        this.evaluatedDifficulty = evaluatedDifficulty;
        this.cTestTokens = cTestTokens;
        this.fetchedFromEvaluations = fetchedFromEvaluations;
    }

    public CTestResponse(Long id, String name, Integer nrOfGaps, Language language, String type,
                         int estimatedDifficulty, Float calculatedDifficulty, Float evaluatedDifficulty,
                         Set<CTestToken> cTestTokens, Set<CTestEvaluation> cTestEvaluations) {
        super();
        this.id = id;
        this.name = name;
        this.nrOfGaps = nrOfGaps;
        this.language = language.getLanguage();
        this.languageCode = language.getLanguageCode();
        this.type = type;
        this.estimatedDifficulty = estimatedDifficulty;
        this.calculatedDifficulty = calculatedDifficulty;
        this.evaluatedDifficulty = evaluatedDifficulty;
        this.cTestTokens = cTestTokens;
        this.cTestEvaluations = cTestEvaluations;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNrOfGaps() {
        return nrOfGaps;
    }

    public void setNrOfGaps(Integer nrOfGaps) {
        this.nrOfGaps = nrOfGaps;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getEstimatedDifficulty() {
        return estimatedDifficulty;
    }

    public void setEstimatedDifficulty(int estimatedDifficulty) {
        this.estimatedDifficulty = estimatedDifficulty;
    }

    public Float getCalculatedDifficulty() {
        return calculatedDifficulty;
    }

    public void setCalculatedDifficulty(Float calculatedDifficulty) {
        this.calculatedDifficulty = calculatedDifficulty;
    }

    public Float getEvaluatedDifficulty() {
        return evaluatedDifficulty;
    }

    public void setEvaluatedDifficulty(Float evaluatedDifficulty) {
        this.evaluatedDifficulty = evaluatedDifficulty;
    }

    public Set<CTestToken> getcTestTokens() {
        return cTestTokens;
    }

    public void setcTestTokens(Set<CTestToken> cTestTokens) {
        this.cTestTokens = cTestTokens;
    }

    public Set<CTestEvaluation> getcTestEvaluations() {
        return cTestEvaluations;
    }

    public void setcTestEvaluations(Set<CTestEvaluation> cTestEvaluations) {
        this.cTestEvaluations = cTestEvaluations;
    }

    public boolean isFetchedFromEvaluations() {
        return fetchedFromEvaluations;
    }

    public void setFetchedFromEvaluations(boolean fetchedFromEvaluations) {
        this.fetchedFromEvaluations = fetchedFromEvaluations;
    }
}
