package de.fopro.ctest.payload.common;

public class LanguageLevelMapping {

    private int estimatedLanguageLevel;
    private Float minLanguageLevelEvaluated;
    private Float maxLanguageLevelEvaluated;

    public LanguageLevelMapping(int estimatedLanguageLevel) {
        this.estimatedLanguageLevel = estimatedLanguageLevel;
        this.setEvaluatedLanguageLevel(estimatedLanguageLevel);
    }

    public int getEstimatedLanguageLevel() {
        return estimatedLanguageLevel;
    }

    public void setEstimatedLanguageLevel(int estimatedLanguageLevel) {
        this.estimatedLanguageLevel = estimatedLanguageLevel;
    }

    public Float getMinLanguageLevelEvaluated() {
        return minLanguageLevelEvaluated;
    }

    public void setMinLanguageLevelEvaluated(Float minLanguageLevelEvaluated) {
        this.minLanguageLevelEvaluated = minLanguageLevelEvaluated;
    }

    public Float getMaxLanguageLevelEvaluated() {
        return maxLanguageLevelEvaluated;
    }

    public void setMaxLanguageLevelEvaluated(Float maxLanguageLevelEvaluated) {
        this.maxLanguageLevelEvaluated = maxLanguageLevelEvaluated;
    }

    private void setEvaluatedLanguageLevel(int estimatedLanguageLevel) {
        switch(estimatedLanguageLevel) {
            case 1:
                setMinLanguageLevelEvaluated(0F);
                setMaxLanguageLevelEvaluated(1/6F);
                break;
            case 2:
                setMinLanguageLevelEvaluated(1/6F);
                setMaxLanguageLevelEvaluated(2/6F);
                break;
            case 3:
                setMinLanguageLevelEvaluated(2/6F);
                setMaxLanguageLevelEvaluated(3/6F);
                break;
            case 4:
                setMinLanguageLevelEvaluated(3/6F);
                setMaxLanguageLevelEvaluated(4/6F);
                break;
            case 5:
                setMinLanguageLevelEvaluated(4/6F);
                setMaxLanguageLevelEvaluated(5/6F);
                break;
            case 6:
                setMinLanguageLevelEvaluated(5/6F);
                setMaxLanguageLevelEvaluated(1F);
                break;
            default:
                setMinLanguageLevelEvaluated(2/6F);
                setMaxLanguageLevelEvaluated(3/6F);
        }
    }

    @Override
    public String toString() {
        return "LanguageLevelMapping{" +
                "estimatedLanguageLevel=" + estimatedLanguageLevel +
                ", minLanguageLevelEvaluated=" + minLanguageLevelEvaluated +
                ", maxLanguageLevelEvaluated=" + maxLanguageLevelEvaluated +
                '}';
    }
}
