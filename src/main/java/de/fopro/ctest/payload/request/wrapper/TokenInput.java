package de.fopro.ctest.payload.request.wrapper;

public class TokenInput {

    private Integer tokenId;
    private String gapValue;

    public Integer getTokenId() {
        return tokenId;
    }

    public void setTokenId(Integer tokenId) {
        this.tokenId = tokenId;
    }

    public String getGapValue() {
        return gapValue;
    }

    public void setGapValue(String gapValue) {
        this.gapValue = gapValue;
    }

    @Override
    public String toString() {
        return "TokenInput{" +
                "tokenId=" + tokenId +
                ", gapValue='" + gapValue + '\'' +
                '}';
    }
}
