package de.fopro.ctest.payload.request.wrapper;

public class UserResult {

  boolean isTestCorrect;
  float scoreOnTest;
  float comparedScore;
  int estimatedLanguageLevel;

  public UserResult() {}

  public UserResult(boolean isTestCorrect, float scoreOnTest, float comparedScore) {
    this.isTestCorrect = isTestCorrect;
    this.scoreOnTest = scoreOnTest;
    this.comparedScore = comparedScore;
    this.estimatedLanguageLevel = estimatedLanguageLevel;
  }

  public boolean isTestCorrect() {
    return isTestCorrect;
  }

  public void setTestCorrect(boolean testCorrect) {
    isTestCorrect = testCorrect;
  }

  public float getScoreOnTest() {
    return scoreOnTest;
  }

  public void setScoreOnTest(float scoreOnTest) {
    this.scoreOnTest = scoreOnTest;
  }

  public float getComparedScore() {
    return comparedScore;
  }

  public void setComparedScore(float comparedScore) {
    this.comparedScore = comparedScore;
  }

  public int getEstimatedLanguageLevel() {
    return estimatedLanguageLevel;
  }

  public void setEstimatedLanguageLevel(int estimatedLanguageLevel) {
    this.estimatedLanguageLevel = estimatedLanguageLevel;
  }
}
