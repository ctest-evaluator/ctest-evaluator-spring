package de.fopro.ctest.payload.request;

public class CreateCTestRequest {
	private String name;
	private String language;
	private String type;
	private int estimatedDifficulty;
	private String cTestContent;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getLanguage() {
		return language;
	}
	
	public void setLanguage(String language) {
		this.language = language;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getcTestContent() {
		return cTestContent;
	}
	
	public void setcTestContent(String cTestContent) {
		this.cTestContent = cTestContent;
	}
	
	public int getEstimatedDifficulty() {
		return estimatedDifficulty;
	}
	
	public void setEstimatedDifficulty(int estimatedDifficulty) {
		this.estimatedDifficulty = estimatedDifficulty;
	}
	

}
