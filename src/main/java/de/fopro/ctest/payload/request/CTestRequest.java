package de.fopro.ctest.payload.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class CTestRequest {
	@NotBlank
	@Size(min = 1, max = 255)
	private String name;

	@NotBlank
	@Size(min = 1, max = 40)
	private String language;
	
	@NotBlank
	@Size(max = 50)
	private String type;
	
	private Float estimatedDifficulty;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Float getEstimatedDifficulty() {
		return estimatedDifficulty;
	}

	public void setEstimatedDifficulty(Float estimatedDifficulty) {
		this.estimatedDifficulty = estimatedDifficulty;
	}
	
	
}
