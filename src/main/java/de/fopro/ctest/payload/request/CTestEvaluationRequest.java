package de.fopro.ctest.payload.request;

import de.fopro.ctest.payload.request.wrapper.TokenInput;
import de.fopro.ctest.payload.request.wrapper.UserData;

import java.util.List;

public class CTestEvaluationRequest {

    private UserData userData;
    private List<TokenInput> tokenInputs;

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    public List<TokenInput> getTokenInputs() {
        return tokenInputs;
    }

    public void setTokenInputs(List<TokenInput> tokenInputs) {
        this.tokenInputs = tokenInputs;
    }

    @Override
    public String toString() {
        return "CTestEvaluationRequest{" +
                "userData=" + userData +
                ", tokenInputs=" + tokenInputs +
                '}';
    }
}
