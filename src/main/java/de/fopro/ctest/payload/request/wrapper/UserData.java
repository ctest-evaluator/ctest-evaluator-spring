package de.fopro.ctest.payload.request.wrapper;

public class UserData {

  private String trainedLanguage;
  private int selfEstimatedLanguageLevel;
  private int currentLanguageLevel;
  private int age;
  private String nativeLanguage;

  public String getTrainedLanguage() {
    return trainedLanguage;
  }

  public void setTrainedLanguage(String trainedLanguage) {
    this.trainedLanguage = trainedLanguage;
  }

  public int getSelfEstimatedLanguageLevel() {
    return selfEstimatedLanguageLevel;
  }

  public void setSelfEstimatedLanguageLevel(int selfEstimatedLanguageLevel) {
    this.selfEstimatedLanguageLevel = selfEstimatedLanguageLevel;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public String getNativeLanguage() {
    return nativeLanguage;
  }

  public void setNativeLanguage(String nativeLanguage) {
    this.nativeLanguage = nativeLanguage;
  }

  public int getCurrentLanguageLevel() {
    return currentLanguageLevel;
  }

  public void setCurrentLanguageLevel(int currentLanguageLevel) {
    this.currentLanguageLevel = currentLanguageLevel;
  }

  @Override
  public String toString() {
    return "UserData{"
        + "trainedLanguage='"
        + trainedLanguage
        + '\''
        + ", selfEstimatedLanguageLevel="
        + selfEstimatedLanguageLevel
        + ", age="
        + age
        + ", nativeLanguage='"
        + nativeLanguage
        + '\''
        + '}';
  }
}
