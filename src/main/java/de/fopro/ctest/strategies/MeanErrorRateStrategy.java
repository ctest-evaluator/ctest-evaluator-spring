package de.fopro.ctest.strategies;

import de.fopro.ctest.models.CTest;
import de.fopro.ctest.models.CTestEvaluation;
import de.fopro.ctest.models.CTestToken;
import de.fopro.ctest.models.CTestTokenMistake;
import de.fopro.ctest.payload.request.wrapper.UserResult;
import de.fopro.ctest.payload.request.wrapper.UserData;
import de.fopro.ctest.repository.CTestEvaluationRepository;
import de.fopro.ctest.repository.CTestRepository;
import de.fopro.ctest.repository.CTestTokenMistakeRepository;
import de.fopro.ctest.repository.CTestTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Service
public class MeanErrorRateStrategy implements DifficultyCalculationStrategy {

    @Autowired
    CTestRepository ctestRepository;
    @Autowired
    CTestTokenRepository ctestTokenRepository;
    @Autowired
    CTestEvaluationRepository cTestEvaluationRepository;
    @Autowired
    CTestTokenMistakeRepository cTestTokenMistakeRepository;

    @Override
    public Float calculateTestDifficulty(CTest ctest, UserResult userResult, UserData userData) {
        List<CTestToken> tokens = ctestTokenRepository.findByctest_Id(ctest.getId());
        double evaluatedDifficulty = tokens.stream()
                .filter(token -> token.getGapStatus() == true)
                .mapToDouble(token -> token.getEvaluatedGapDifficulty()).sum();

        return (float)  evaluatedDifficulty / ctest.getNrOfGaps();
    }

    @Override
    public Float calculateGapDifficulty(CTest ctest, CTestToken cTestToken) {
        List<CTestEvaluation> evaluations = cTestEvaluationRepository.findByctest_Id(ctest.getId());
        int evaluationsCount = !ObjectUtils.isEmpty(evaluations) ? evaluations.size() : 1;

        List<CTestTokenMistake> mistakes = cTestTokenMistakeRepository.findByctestToken_Id(cTestToken.getId());
        int tokenMistakesCount = 0;
        for (CTestTokenMistake mistake : mistakes) {
            tokenMistakesCount = tokenMistakesCount + mistake.getCount();
        }

        return Float.valueOf(tokenMistakesCount) / Float.valueOf(evaluationsCount);
    }

    @Override
    public int calculateUserDifficulty(UserResult userResult, UserData userData, CTest ctest) {
        int estimatedLanguageLevel = userData.getCurrentLanguageLevel();

        if(userResult.getScoreOnTest() >= 85 && estimatedLanguageLevel < 6) {
            estimatedLanguageLevel++;
        }else if (userResult.getScoreOnTest() <= 15 && estimatedLanguageLevel > 1){
            estimatedLanguageLevel--;
        }
        return estimatedLanguageLevel;
    }

    @Override
    public StrategyName getStrategyName() {
        return StrategyName.MEAN_ERROR_RATE;
    }



}
