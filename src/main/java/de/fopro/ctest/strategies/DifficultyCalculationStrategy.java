package de.fopro.ctest.strategies;

import de.fopro.ctest.models.CTest;
import de.fopro.ctest.models.CTestToken;
import de.fopro.ctest.payload.request.wrapper.UserResult;
import de.fopro.ctest.payload.request.wrapper.UserData;

public interface DifficultyCalculationStrategy {

    Float calculateGapDifficulty(CTest ctest, CTestToken cTestToken);
    Float calculateTestDifficulty(CTest ctest, UserResult userResult, UserData userData);
    StrategyName getStrategyName();
	int calculateUserDifficulty(UserResult userResult, UserData userData, CTest ctest);
}
