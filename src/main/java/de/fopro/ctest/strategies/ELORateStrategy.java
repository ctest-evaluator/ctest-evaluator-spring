package de.fopro.ctest.strategies;

import de.fopro.ctest.models.CTest;
import de.fopro.ctest.models.CTestEvaluation;
import de.fopro.ctest.models.CTestToken;
import de.fopro.ctest.models.CTestTokenMistake;
import de.fopro.ctest.payload.request.wrapper.UserResult;
import de.fopro.ctest.payload.request.wrapper.UserData;
import de.fopro.ctest.repository.CTestEvaluationRepository;
import de.fopro.ctest.repository.CTestRepository;
import de.fopro.ctest.repository.CTestTokenMistakeRepository;
import de.fopro.ctest.repository.CTestTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Service
public class ELORateStrategy implements DifficultyCalculationStrategy{
    @Autowired
    CTestRepository ctestRepository;
    @Autowired
    CTestTokenRepository ctestTokenRepository;
    @Autowired
    CTestEvaluationRepository cTestEvaluationRepository;
    @Autowired
    CTestTokenMistakeRepository cTestTokenMistakeRepository;
	
    

	@Override
	public Float calculateGapDifficulty(CTest ctest, CTestToken cTestToken) {
        List<CTestEvaluation> evaluations = cTestEvaluationRepository.findByctest_Id(ctest.getId());
        int evaluationsCount = !ObjectUtils.isEmpty(evaluations) ? evaluations.size() : 1;

        List<CTestTokenMistake> mistakes = cTestTokenMistakeRepository.findByctestToken_Id(cTestToken.getId());
        int tokenMistakesCount = 0;
        for (CTestTokenMistake mistake : mistakes) {
            tokenMistakesCount = tokenMistakesCount + mistake.getCount();
        }

        return Float.valueOf(tokenMistakesCount) / Float.valueOf(evaluationsCount);
	}

	@Override
	public int calculateUserDifficulty(UserResult userResult, UserData userData, CTest ctest) {
		float cTestOldRating = ctest.getEvaluatedDifficulty() * 6;
		if(cTestOldRating == 0) {
			cTestOldRating = ctest.getEstimatedDifficulty();
		}
		float userDataOldRating = userData.getCurrentLanguageLevel();
		float expectedWinRateUser = (float) (1/(1+Math.pow(10, (cTestOldRating-userDataOldRating))));
		int userWin;
		
		if(userResult.getScoreOnTest() >= 50) {
			userWin=1;
		}else {
			userWin=0;
		}
		
		float result = userDataOldRating + (userWin - expectedWinRateUser);
		if(result <= 1) {
			result =1;
		}if(result >= 6) {
			result =6;
		}
		return (int) result;
	}

	@Override
	public StrategyName getStrategyName() {
		// TODO Auto-generated method stub
		return StrategyName.ELO_RATE;
	}

	@Override
	public Float calculateTestDifficulty(CTest ctest, UserResult userResult, UserData userData) {
		float cTestOldRating = ctest.getEvaluatedDifficulty() * 6;
		float userDataOldRating = userData.getCurrentLanguageLevel();
		if(cTestOldRating == 0) {
			cTestOldRating = ctest.getEstimatedDifficulty();
		}
		float expectedWinRateCTest = (float) (1/(1+Math.pow(10, (userDataOldRating-cTestOldRating))));
		int userWin;
		
		if(userResult.getScoreOnTest() >= 50) {
			userWin=0;
		}else {
			userWin=1;
		}
		
		float result = cTestOldRating + (userWin - expectedWinRateCTest);
		if(result <= 1) {
			result =1;
		}if(result >= 6) {
			result =6;
		}
		return result/6;
	}



}
