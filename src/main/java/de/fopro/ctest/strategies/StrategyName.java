package de.fopro.ctest.strategies;

public enum StrategyName {
	ELO_RATE("eloRate"),
	MEAN_ERROR_RATE("meanErrorRate");

    private final String name;

    StrategyName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
