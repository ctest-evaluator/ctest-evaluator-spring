package de.fopro.ctest.strategies;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
public class DifficultyCalculationFactory {

    private Map<StrategyName, DifficultyCalculationStrategy> strategies;

    DifficultyCalculationFactory(Set<DifficultyCalculationStrategy> strategySet) {
        createStrategies(strategySet);
    }

    public DifficultyCalculationStrategy findStrategy(StrategyName strategyName) {
        return strategies.get(strategyName);
    }

    private void createStrategies(Set<DifficultyCalculationStrategy> strategySet) {
        strategies = new HashMap<StrategyName, DifficultyCalculationStrategy>();
        strategySet.forEach(
                strategy -> strategies.put(strategy.getStrategyName(), strategy)
        );
    }
}
