package de.fopro.ctest.controllers;

import de.fopro.ctest.models.Language;
import de.fopro.ctest.repository.LanguageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class LanguageController {

    @Autowired
    LanguageRepository languageRepository;

    @GetMapping("/api/languages")
    public ResponseEntity getLanguages() {
        List<Language> languages = languageRepository.findAll();
        if (!ObjectUtils.isEmpty(languages)) {
            return ResponseEntity.ok(languages);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
