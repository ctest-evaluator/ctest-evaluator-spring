package de.fopro.ctest.controllers;

import de.fopro.ctest.models.CTest;
import de.fopro.ctest.models.Language;
import de.fopro.ctest.payload.common.LanguageLevelMapping;
import de.fopro.ctest.payload.request.CreateCTestRequest;
import de.fopro.ctest.payload.response.CTestResponse;
import de.fopro.ctest.services.CTestsService;
import de.fopro.ctest.services.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class CTestController {

  private CTestsService ctestsService;
  private LanguageService languageService;

  @Autowired
  public void setProductService(CTestsService cTestsService) {
    this.ctestsService = cTestsService;
  }

  @Autowired
  public void setLanguageService(LanguageService languageService) {
    this.languageService = languageService;
  }

  @PostMapping("/api/ctests/import")
  public ResponseEntity importCTest(
      @Valid @RequestParam(value = "name", required = true) String name,
      @RequestParam(value = "language", required = true) String languageCode,
      @RequestParam(value = "type", required = true) String type,
      @RequestParam(value = "estimatedDifficulty", required = true) int estimatedDifficulty,
      @RequestPart(value = "file", required = true) final MultipartFile file) {
    Optional<CTest> ctestOptional =
        ctestsService.saveCtest(file, name, type, estimatedDifficulty, languageCode);
    if (ctestOptional.isEmpty()) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

    CTest ctest = ctestOptional.get();
    return ResponseEntity.ok(
        new CTestResponse(
            ctest.getId(),
            ctest.getName(),
            ctest.getNrOfGaps(),
            ctest.getLanguage(),
            ctest.getType(),
            ctest.getEstimatedDifficulty(),
            ctest.getCalculatedDifficulty(),
            ctest.getEvaluatedDifficulty(),
            ctest.getcTestTokens()));
  }

  @PostMapping("/api/ctests/create")
  public ResponseEntity createCTest(@RequestBody CreateCTestRequest createCtest) {
    Optional<CTest> ctestOptional =
        ctestsService.saveCtest(
            createCtest.getcTestContent(),
            createCtest.getName(),
            createCtest.getType(),
            createCtest.getEstimatedDifficulty(),
            createCtest.getLanguage());
    if (ctestOptional.isEmpty()) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    CTest ctest = ctestOptional.get();

    return ResponseEntity.ok(
        new CTestResponse(
            ctest.getId(),
            ctest.getName(),
            ctest.getNrOfGaps(),
            ctest.getLanguage(),
            ctest.getType(),
            ctest.getEstimatedDifficulty(),
            ctest.getCalculatedDifficulty(),
            ctest.getEvaluatedDifficulty(),
            ctest.getcTestTokens()));
  }

  @GetMapping("/api/ctest")
  public ResponseEntity getCTestsByLanguageAndLevel(
      @RequestParam(value = "languageCode") Optional<String> languageCodeOptional,
      @RequestParam(value = "completedCTests") Optional<Long[]> completedCTestsOptional,
      @RequestParam(value = "languageLevel") Optional<Integer> userLanguageLevelOptional) {
    if (languageCodeOptional.isEmpty() || StringUtils.isEmpty(languageCodeOptional.get()))
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    if (userLanguageLevelOptional.isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    Long[] completedCTests =
        completedCTestsOptional.isPresent() ? completedCTestsOptional.get() : new Long[] {0L};
    Language language = languageService.getLanguageByLanguageCode(languageCodeOptional.get());
    LanguageLevelMapping languageLevel = new LanguageLevelMapping(userLanguageLevelOptional.get());

    boolean fetchedFromEvaluations = false;
    Optional<CTest> ctestOptional =
        ctestsService.getCTestByUserDataByEstimatedDifficulty(
            completedCTests, language, languageLevel);
    if (ctestOptional.isPresent()) {
      CTest ctest = ctestOptional.get();
      return ResponseEntity.ok(
          new CTestResponse(
              ctest.getId(),
              ctest.getName(),
              ctest.getNrOfGaps(),
              ctest.getLanguage(),
              ctest.getType(),
              ctest.getEstimatedDifficulty(),
              ctest.getCalculatedDifficulty(),
              ctest.getEvaluatedDifficulty(),
              ctest.getcTestTokens(),
              fetchedFromEvaluations));
    } else {
      ctestOptional =
          ctestsService.getCTestByUserDataByEvaluatedDifficulty(
              completedCTests, language, languageLevel);
      if (ctestOptional.isPresent()) {
        fetchedFromEvaluations = true;
        CTest ctest = ctestOptional.get();
        return ResponseEntity.ok(
            new CTestResponse(
                ctest.getId(),
                ctest.getName(),
                ctest.getNrOfGaps(),
                ctest.getLanguage(),
                ctest.getType(),
                ctest.getEstimatedDifficulty(),
                ctest.getCalculatedDifficulty(),
                ctest.getEvaluatedDifficulty(),
                ctest.getcTestTokens(),
                fetchedFromEvaluations));
      }
    }
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

  @GetMapping("/api/ctests/random")
  public ResponseEntity<CTestResponse> getCTestRandom() {
    Optional<CTest> ctestOptional = ctestsService.getRandomCtest();
    if (ctestOptional.isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    CTest ctest = ctestOptional.get();
    return ResponseEntity.ok(
        new CTestResponse(
            ctest.getId(),
            ctest.getName(),
            ctest.getNrOfGaps(),
            ctest.getLanguage(),
            ctest.getType(),
            ctest.getEstimatedDifficulty(),
            ctest.getCalculatedDifficulty(),
            ctest.getEvaluatedDifficulty(),
            ctest.getcTestTokens()));
  }

  @GetMapping("/api/ctests/{id}")
  public ResponseEntity<CTestResponse> getCTest(@PathVariable("id") long id) {
    Optional<CTest> cTestOptional = ctestsService.getCTestById(id);
    if (cTestOptional.isPresent()) {
      CTest ctest = cTestOptional.get();
      return ResponseEntity.ok(
          new CTestResponse(
              ctest.getId(),
              ctest.getName(),
              ctest.getNrOfGaps(),
              ctest.getLanguage(),
              ctest.getType(),
              ctest.getEstimatedDifficulty(),
              ctest.getCalculatedDifficulty(),
              ctest.getEvaluatedDifficulty(),
              ctest.getcTestTokens(),
              ctest.getcTestEvaluations()));
    } else {
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
  }

  @GetMapping("/api/ctests")
  public ResponseEntity<List<CTestResponse>> getCTests() {
    List<CTest> ctests = ctestsService.getAllCTests();
    if (ctests.size() > 0) {
      List<CTestResponse> responses = new ArrayList<CTestResponse>();
      for (int i = 0; i < ctests.size(); i++) {
        responses.add(
            new CTestResponse(
                ctests.get(i).getId(),
                ctests.get(i).getName(),
                ctests.get(i).getNrOfGaps(),
                ctests.get(i).getLanguage(),
                ctests.get(i).getType(),
                ctests.get(i).getEstimatedDifficulty(),
                ctests.get(i).getCalculatedDifficulty(),
                ctests.get(i).getEvaluatedDifficulty(),
                ctests.get(i).getcTestTokens(),
                ctests.get(i).getcTestEvaluations()));
      }

      return ResponseEntity.ok(responses);
    } else {
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
  }

  @DeleteMapping("/api/ctests/{id}")
  public ResponseEntity deleteCtest(@PathVariable("id") long id) {
    if (ctestsService.deleteCTestById(id)) {
      return new ResponseEntity<>(HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
  }
}
