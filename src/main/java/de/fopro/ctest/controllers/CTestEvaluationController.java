package de.fopro.ctest.controllers;

import de.fopro.ctest.payload.request.CTestEvaluationRequest;
import de.fopro.ctest.payload.response.CTestEvaluationResponse;
import de.fopro.ctest.services.CTestEvaluationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CTestEvaluationController {

  private CTestEvaluationService evaluationService;

  @Autowired
  public void setEvaluationService(CTestEvaluationService evaluationService) {
    this.evaluationService = evaluationService;
  }

  @PostMapping(value = "/api/ctest/evaluate/{id}")
  public ResponseEntity<CTestEvaluationResponse> evaluateCTest(
      @PathVariable("id") long id, @RequestBody CTestEvaluationRequest ctestEvaluationRequest) {
      CTestEvaluationResponse evaluationResponse =
          evaluationService.evaluateUserOnCTest(id, ctestEvaluationRequest);
      return ResponseEntity.ok(evaluationResponse);
  }
}
