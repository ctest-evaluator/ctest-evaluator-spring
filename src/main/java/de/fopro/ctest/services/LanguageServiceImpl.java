package de.fopro.ctest.services;

import de.fopro.ctest.models.Language;
import de.fopro.ctest.repository.LanguageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LanguageServiceImpl implements LanguageService{

    @Autowired
    LanguageRepository languageRepository;

    @Override
    public Language getLanguageByLanguageCode(String languageCode) {
        return languageRepository.findByLanguageCode(languageCode);
    }
}
