package de.fopro.ctest.services;

import de.fopro.ctest.models.Language;

public interface LanguageService {

    public Language getLanguageByLanguageCode(String languageCode);
}
