package de.fopro.ctest.services;

import de.fopro.ctest.models.CTest;
import de.fopro.ctest.models.Language;
import de.fopro.ctest.payload.common.LanguageLevelMapping;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

public interface CTestsService {

    public Optional<CTest> saveCtest(MultipartFile file, String name, String type, int estimatedDifficulty, String languageCode);
    
    public Optional<CTest> saveCtest(String cTestContent, String name, String type, int estimatedDifficulty, String languageCode);

    public Optional<CTest> getCTestByUserData(Long[] completedCTests, Language language, LanguageLevelMapping languageLevel);

    public Optional<CTest> getCTestByUserDataByEstimatedDifficulty(Long[] completedCTests, Language language, LanguageLevelMapping languageLevel);

    public Optional<CTest> getCTestByUserDataByEvaluatedDifficulty(Long[] completedCTests, Language language, LanguageLevelMapping languageLevel);

    public Optional<CTest> getRandomCtest();

    Optional<CTest> getCTestById(long id);

    List<CTest> getAllCTests();

    boolean deleteCTestById(long id);

}
