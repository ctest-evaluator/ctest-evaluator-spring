package de.fopro.ctest.services;

import de.fopro.ctest.models.CTest;
import de.fopro.ctest.models.CTestEvaluation;
import de.fopro.ctest.models.CTestToken;
import de.fopro.ctest.models.CTestTokenMistake;
import de.fopro.ctest.payload.request.wrapper.UserResult;
import de.fopro.ctest.payload.request.CTestEvaluationRequest;
import de.fopro.ctest.payload.request.wrapper.TokenInput;
import de.fopro.ctest.payload.request.wrapper.UserData;
import de.fopro.ctest.payload.response.CTestEvaluationResponse;
import de.fopro.ctest.payload.response.wrapper.CTestEvaluationTokenResponse;
import de.fopro.ctest.repository.CTestEvaluationRepository;
import de.fopro.ctest.repository.CTestRepository;
import de.fopro.ctest.repository.CTestTokenMistakeRepository;
import de.fopro.ctest.repository.CTestTokenRepository;
import de.fopro.ctest.strategies.DifficultyCalculationFactory;
import de.fopro.ctest.strategies.StrategyName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CTestEvaluationServiceImpl implements CTestEvaluationService {
  private final StrategyName calculationStrategy = StrategyName.MEAN_ERROR_RATE;

  @Autowired CTestRepository ctestRepository;
  @Autowired CTestEvaluationRepository cTestEvaluationRepository;
  @Autowired CTestTokenRepository ctestTokenRepository;
  @Autowired CTestTokenMistakeRepository cTestTokenMistakeRepository;

  @Autowired private DifficultyCalculationFactory difficultyCalculationFactory;

  public CTestEvaluationResponse evaluateUserOnCTest(
      Long ctestId, CTestEvaluationRequest ctestEvaluationRequest) {
	  
    CTest ctest = ctestRepository.findById(ctestId).get();
    List<CTestEvaluation> allEvaluations = cTestEvaluationRepository.findByctest_Id(ctest.getId());
    long totalAmount = allEvaluations.size();

    UserData userData = ctestEvaluationRequest.getUserData();
    CTestEvaluation evaluation =
        new CTestEvaluation(
            ctest,
            0,
            userData.getTrainedLanguage(),
            userData.getSelfEstimatedLanguageLevel(),
            userData.getAge(),
            userData.getNativeLanguage());
    evaluation = cTestEvaluationRepository.save(evaluation);
    
    List<CTestEvaluationTokenResponse> tokenResponses =
            evaluateUserOnGaps(evaluation, ctestEvaluationRequest, ctest);
    
    UserResult userResult = new UserResult();
    isUserCorrectOnCTest(userResult, evaluation, ctest);
    calculateComparedScoring(userResult, allEvaluations, evaluation, totalAmount);
    calculateScoreOnTest(userResult, evaluation, ctest);


    updateUserAndTestDifficulty(ctest, userResult, userData);
    ctestRepository.save(ctest);
    cTestEvaluationRepository.save(evaluation);

    return new CTestEvaluationResponse(
        ctest.getId(),
        tokenResponses,
        userResult.getEstimatedLanguageLevel(),
        evaluation.getNumberCorrectGaps(),
        userResult.isTestCorrect(),
        userResult.getComparedScore());
  }

  private List<CTestEvaluationTokenResponse> evaluateUserOnGaps(
      CTestEvaluation evaluation, CTestEvaluationRequest ctestEvaluationRequest, CTest ctest) {
    List<CTestEvaluationTokenResponse> tokenResponses =
        new ArrayList<CTestEvaluationTokenResponse>();
    int numberCorrectGaps = 0;

    for (TokenInput tokenInput : ctestEvaluationRequest.getTokenInputs()) {
      Optional<CTestToken> tokenOptional =
          ctestTokenRepository.findById(Long.valueOf(tokenInput.getTokenId()));
      CTestToken ctestToken = tokenOptional.get();

      // compare request input token with actual gap value: token->value->offset
      String gapValue = tokenInput.getGapValue();
      if (!gapValue.equals(ctestToken.getValue().substring(ctestToken.getOffset()))) {
        updateTokenMistakeCount(ctestToken, gapValue);
        updateGapDifficulty(ctest, ctestToken);

        CTestToken updatedCTestToken = ctestTokenRepository.save(ctestToken);
        tokenResponses.add(new CTestEvaluationTokenResponse(updatedCTestToken, gapValue, false));
      } else {
        numberCorrectGaps++;
        updateGapDifficulty(ctest, ctestToken);

        CTestToken updatedCTestToken = ctestTokenRepository.save(ctestToken);
        tokenResponses.add(new CTestEvaluationTokenResponse(updatedCTestToken, gapValue, true));
      }
    }
    evaluation.setNumberCorrectGaps(numberCorrectGaps);
    return tokenResponses;
  }

  private void updateTokenMistakeCount(CTestToken ctestToken, String gapValue) {
    CTestTokenMistake mistake =
        cTestTokenMistakeRepository
            .findByctestToken_IdAndValue(ctestToken.getId(), gapValue)
            .orElse(new CTestTokenMistake(ctestToken, gapValue, 0));
    mistake.setCount(mistake.getCount() + 1);
    cTestTokenMistakeRepository.save(mistake);
  }

  private void isUserCorrectOnCTest(
      UserResult userResult, CTestEvaluation evaluation, CTest ctest) {
    boolean isTestCorrect =
        (ctest.getNrOfGaps() - evaluation.getNumberCorrectGaps()) <= (ctest.getNrOfGaps() / 2);
    userResult.setTestCorrect(isTestCorrect);
  }

  private void calculateComparedScoring(
      UserResult userResult,
      List<CTestEvaluation> allEvaluations,
      CTestEvaluation evaluation,
      long totalAmount) {
    float scoring = 100f;
    long amountLowerThanUser =
        allEvaluations.stream()
            .filter(e -> e.getNumberCorrectGaps() < evaluation.getNumberCorrectGaps())
            .count();

    if (allEvaluations.size() > 1)
      scoring = (amountLowerThanUser / Float.valueOf(totalAmount)) * 100;
    userResult.setComparedScore(scoring);
  }

  private void calculateScoreOnTest(
      UserResult userResult, CTestEvaluation evaluation, CTest ctest) {
    Float scoring =
        (Float.valueOf(evaluation.getNumberCorrectGaps()) / Float.valueOf(ctest.getNrOfGaps()))
            * 100;
    userResult.setScoreOnTest(scoring.floatValue());
  }

  private void updateGapDifficulty(CTest ctest, CTestToken ctestToken) {
    Float updatedGapDifficulty =
        difficultyCalculationFactory
            .findStrategy(calculationStrategy)
            .calculateGapDifficulty(ctest, ctestToken);
    ctestToken.setEvaluatedGapDifficulty(updatedGapDifficulty);
  }
  private void updateUserAndTestDifficulty(CTest ctest, UserResult userResult, UserData userData) {
	  		Float updatedTestDifficulty = difficultyCalculationFactory
			  .findStrategy(calculationStrategy)
			  .calculateTestDifficulty(ctest, userResult, userData);
		    int updatedUserDifficulty = difficultyCalculationFactory
		    		.findStrategy(calculationStrategy)
		    		.calculateUserDifficulty(userResult, userData, ctest);
		    userResult.setEstimatedLanguageLevel(updatedUserDifficulty);
		    ctest.setEvaluatedDifficulty(updatedTestDifficulty);
	  
  }

  private void updateTestDifficulty(CTest ctest, UserResult userResult, UserData userData) {
    Float updatedTestDifficulty =
        difficultyCalculationFactory
            .findStrategy(calculationStrategy)
            .calculateTestDifficulty(ctest, userResult, userData);
    ctest.setEvaluatedDifficulty(updatedTestDifficulty);
  }

  private void updateUserDifficulty(UserResult userResult, UserData userData, CTest ctest) {
    int updatedUserDifficulty =
        difficultyCalculationFactory
            .findStrategy(calculationStrategy)
            .calculateUserDifficulty(userResult, userData, ctest);
    userResult.setEstimatedLanguageLevel(updatedUserDifficulty);
  }
}
