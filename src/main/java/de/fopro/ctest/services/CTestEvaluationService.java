package de.fopro.ctest.services;

import de.fopro.ctest.payload.request.CTestEvaluationRequest;
import de.fopro.ctest.payload.response.CTestEvaluationResponse;

public interface CTestEvaluationService {
  public CTestEvaluationResponse evaluateUserOnCTest(
      Long ctestId, CTestEvaluationRequest ctestEvaluationRequest);
}
