package de.fopro.ctest.services;

import de.fopro.ctest.models.CTest;
import de.fopro.ctest.models.CTestToken;
import de.fopro.ctest.models.Language;
import de.fopro.ctest.payload.common.LanguageLevelMapping;
import de.fopro.ctest.repository.CTestEvaluationRepository;
import de.fopro.ctest.repository.CTestRepository;
import de.fopro.ctest.repository.CTestTokenRepository;
import de.fopro.ctest.repository.LanguageRepository;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class CTestsServiceImpl implements CTestsService {

  @Autowired CTestRepository ctestRepository;
  @Autowired CTestTokenRepository ctestTokenRepository;
  @Autowired CTestEvaluationRepository cTestEvaluationRepository;
  @Autowired LanguageRepository languageRepository;

  public Optional<CTest> saveCtest(
      MultipartFile file, String name, String type, int estimatedDifficulty, String languageCode) {
    Language ctestLanguage = languageRepository.findByLanguageCode(languageCode);
    try {
      JSONObject parsedCTestTokensFile = parseCTestTokenFileRequest(file);
      JSONArray ctestTokens = (JSONArray) parsedCTestTokensFile.get("words");

      CTest ctest =
          new CTest(
              name,
              ctestLanguage,
              type,
              0,
              estimatedDifficulty,
              Float.valueOf(0F),
              Float.valueOf(0F));
      CTest savedCtest = ctestRepository.save(ctest);

      int nrOfGaps = 0;
      for (int i = 0; i < ctestTokens.length(); i++) {
        JSONObject ctestTokenJSON = (JSONObject) ctestTokens.get(i);
        if (ctestTokenJSON.getBoolean("gapStatus")) nrOfGaps++;

        CTestToken ctestToken =
            new CTestToken(
                savedCtest,
                ctestTokenJSON.getString("value"),
                ctestTokenJSON.getInt("offset"),
                ctestTokenJSON.getBoolean("gapStatus"),
                ctestTokenJSON.getBoolean("isNormal"),
                ctestTokenJSON.getFloat("difficulty"),
                Float.valueOf(0F),
                ctestTokenJSON.getBoolean("isLastTokenInSentence"));
        ctestTokenRepository.save(ctestToken);
      }
      ctest.setNrOfGaps(nrOfGaps);
      return Optional.of(ctestRepository.save(ctest));
    } catch (Exception e) {
      e.printStackTrace();
      return Optional.empty();
    }
  }

  public Optional<CTest> saveCtest(
      String cTestContent, String name, String type, int estimatedDifficulty, String languageCode) {
    Language ctestLanguage = languageRepository.findByLanguageCode(languageCode);
    try {

      JSONArray ctestTokens = parseTextToCtest(cTestContent);

      CTest ctest =
          new CTest(
              name,
              ctestLanguage,
              type,
              0,
              estimatedDifficulty,
              Float.valueOf(0F),
              Float.valueOf(0F));
      CTest savedCtest = ctestRepository.save(ctest);

      int nrOfGaps = 0;
      for (int i = 0; i < ctestTokens.length(); i++) {
        JSONObject ctestTokenJSON = (JSONObject) ctestTokens.get(i);
        if (ctestTokenJSON.getBoolean("gapStatus")) nrOfGaps++;

        CTestToken ctestToken =
            new CTestToken(
                savedCtest,
                ctestTokenJSON.getString("value"),
                ctestTokenJSON.getInt("offset"),
                ctestTokenJSON.getBoolean("gapStatus"),
                ctestTokenJSON.getBoolean("isNormal"),
                ctestTokenJSON.getFloat("difficulty"),
                Float.valueOf(0F),
                ctestTokenJSON.getBoolean("isLastTokenInSentence"));
        ctestTokenRepository.save(ctestToken);
      }
      ctest.setNrOfGaps(nrOfGaps);
      return Optional.of(ctestRepository.save(ctest));
    } catch (Exception e) {
      e.printStackTrace();
      return Optional.empty();
    }
  }

  public Optional<CTest> getCTestByUserData(
      Long[] completedCTests, Language language, LanguageLevelMapping languageLevel) {
    List<CTest> ctestsByEstimatedDifficulty =
        ctestRepository.findByEstimatedDifficultyLanguageAndLevel(
            completedCTests, language.getId(), languageLevel.getEstimatedLanguageLevel());

    Optional<CTest> ctestByEstimatedDifficulty =
        ctestsByEstimatedDifficulty.stream()
            .filter(ctest -> ctest != null && ctest.getcTestEvaluations().size() < 10)
            .findAny();

    Optional<CTest> ctest =
        ctestByEstimatedDifficulty.or(
            () -> {
              List<CTest> ctestsByEvaluatedDifficulty =
                  ctestRepository.findByEvaluatedDifficultyLanguageAndLevel(
                      completedCTests,
                      language.getId(),
                      languageLevel.getMinLanguageLevelEvaluated(),
                      languageLevel.getMaxLanguageLevelEvaluated());
              return ctestsByEvaluatedDifficulty.stream().findAny();
            });
    return ctest;
  }

  public Optional<CTest> getCTestByUserDataByEstimatedDifficulty(
      Long[] completedCTests, Language language, LanguageLevelMapping languageLevel) {
    List<CTest> ctestsByEstimatedDifficulty =
        ctestRepository.findByEstimatedDifficultyLanguageAndLevel(
            completedCTests, language.getId(), languageLevel.getEstimatedLanguageLevel());

    Optional<CTest> ctestByEstimatedDifficulty =
        ctestsByEstimatedDifficulty.stream()
            .filter(ctest -> ctest != null && ctest.getcTestEvaluations().size() < 10)
            .findAny();

    return ctestByEstimatedDifficulty;
  }

  public Optional<CTest> getCTestByUserDataByEvaluatedDifficulty(
      Long[] completedCTests, Language language, LanguageLevelMapping languageLevel) {
    List<CTest> ctestsByEvaluatedDifficulty =
        ctestRepository.findByEvaluatedDifficultyLanguageAndLevel(
            completedCTests,
            language.getId(),
            languageLevel.getMinLanguageLevelEvaluated(),
            languageLevel.getMaxLanguageLevelEvaluated());
    return ctestsByEvaluatedDifficulty.stream().findAny();
  }

  public Optional<CTest> getRandomCtest() {
    List<CTest> ctests = ctestRepository.findAll();
    if (ObjectUtils.isEmpty(ctests)) return Optional.empty();

    Random rand = new Random();
    CTest ctest = ctests.get(rand.nextInt(ctests.size()));
    return Optional.of(ctest);
  }

  public Optional<CTest> getCTestById(long id) {
    Optional<CTest> ctestOptional = ctestRepository.findById(id);
    return ctestOptional;
  }

  public List<CTest> getAllCTests() {
    return ctestRepository.findAll();
  }

  public boolean deleteCTestById(long id) {
    Optional<CTest> ctestOptional = ctestRepository.findById(id);
    if (ctestOptional.isPresent()) {
      ctestRepository.deleteById(id);
      return true;
    } else {
      return false;
    }
  }

  private JSONObject parseCTestTokenFileRequest(MultipartFile ctestFile)
      throws JSONException, IOException {
    return new JSONObject(new String(ctestFile.getBytes()));
  }

  private JSONArray parseTextToCtest(String ctestText) {
    JSONArray words = new JSONArray();
    String tokenizeRegex =
    		"[\\p{L}\\p{M}\\p{N}\\{\\}\\-\\']+(?:\\p{P}[\\p{L}\\p{M}\\p{N}]+)*|[\\p{P}\\p{S}]";
    String[] wordList =
        Pattern.compile(tokenizeRegex)
            .matcher(ctestText)
            .results()
            .map(MatchResult::group)
            .toArray(String[]::new);
    Pattern findGapRegex = Pattern.compile("\\{([^)]+)\\}");
    Matcher regexMatcher;

    for (String word : wordList) {
      JSONObject result = new JSONObject();
      regexMatcher = findGapRegex.matcher(word);
      if (regexMatcher.find()) {
        result.put("offset", word.indexOf("{"));
        result.put("gapStatus", true);
        result.put("value", word.replace("{", "").replace("}", ""));
        result.put("isNormal", false);

      } else {
        result.put("gapStatus", false);
        result.put("offset", 0);
        result.put("value", word);
        result.put("isNormal", true);
      }
      result.put("difficulty", -1);
      result.put("isLastTokenInSentence", false);
      words.put(result);
    }

    return words;
  }
}
