package de.fopro.ctest.repository;

import de.fopro.ctest.models.CTestToken;
import de.fopro.ctest.models.CTestTokenMistake;
import de.fopro.ctest.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;

@Repository
public interface CTestTokenMistakeRepository extends JpaRepository<CTestTokenMistake, Long> {
    Optional<CTestTokenMistake> findByValue(String value);

    Optional<CTestTokenMistake> findByctestToken_IdAndValue(Long id, String value);

    List<CTestTokenMistake> findByctestToken_Id(Long id);
}
