package de.fopro.ctest.repository;

import de.fopro.ctest.models.CTestEvaluation;
import de.fopro.ctest.models.CTestToken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CTestEvaluationRepository extends JpaRepository<CTestEvaluation, Long> {

    List<CTestEvaluation> findByctest_Id(Long id);

}
