package de.fopro.ctest.repository;

import de.fopro.ctest.models.CTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CTestRepository extends JpaRepository<CTest, Long> {

//    @Query("SELECT c, count(e) from CTest c join c.cTestEvaluations e WHERE (c.id) NOT IN (:ctestIds) AND (c.language.id) = (:languageId) AND c.estimatedDifficulty = (:languageLevel)")
//    public List<CTest> findByEstimatedDifficultyLanguageAndLevel(
//    		@Param("ctestIds") Long[] ctestIds,
//			@Param("languageId") Long languageId,
//			@Param("languageLevel") int languageLevel);

	@Query("SELECT c from CTest c WHERE (c.id) NOT IN (:ctestIds) AND (c.language.id) = (:languageId) AND c.estimatedDifficulty = (:languageLevel)")
	public List<CTest> findByEstimatedDifficultyLanguageAndLevel(
			@Param("ctestIds") Long[] ctestIds,
			@Param("languageId") Long languageId,
			@Param("languageLevel") int languageLevel);


    @Query("SELECT c from CTest c WHERE (c.id) NOT IN (:ctestIds) AND (c.language.id) = (:languageId) AND (c.evaluatedDifficulty) BETWEEN (:levelMin) and (:levelMax)")
    public List<CTest> findByEvaluatedDifficultyLanguageAndLevel(
    		@Param("ctestIds") Long[] ctestIds,
			@Param("languageId") Long languageId,
			@Param("levelMin") Float levelMin,
			@Param("levelMax") Float levelMax);

	@Query("SELECT c from CTest c WHERE (c.id) NOT IN (:ctestIds) AND (c.language.id) = (:languageId) AND (c.calculatedDifficulty) BETWEEN (:levelMin) and (:levelMax)")
	public List<CTest> findByCalculatedDifficultyLanguageAndLevel(
			@Param("ctestIds") Long[] ctestIds,
			@Param("languageId") Long languageId,
			@Param("levelMin") Float levelMin,
			@Param("levelMax") Float levelMax);

    boolean existsById(Long id);
}
