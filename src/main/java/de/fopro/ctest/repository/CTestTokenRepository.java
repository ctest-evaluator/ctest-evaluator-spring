package de.fopro.ctest.repository;

import de.fopro.ctest.models.CTestEvaluation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import de.fopro.ctest.models.CTestToken;

import java.util.List;

@Repository
public interface CTestTokenRepository extends JpaRepository<CTestToken, Long>{

    List<CTestToken> findByctest_Id(Long id);
}
