package de.fopro.ctest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import de.fopro.ctest.models.Language;

@Repository
public interface LanguageRepository extends JpaRepository<Language, Long>{
	
	@Query("SELECT l from Language l WHERE LOWER(l.languageCode) = LOWER(:code)")
	public Language findByLanguageCode(@Param("code")String code);
}


