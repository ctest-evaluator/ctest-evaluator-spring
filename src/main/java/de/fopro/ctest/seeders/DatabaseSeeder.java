package de.fopro.ctest.seeders;

import de.fopro.ctest.models.Language;
import de.fopro.ctest.models.User;
import de.fopro.ctest.repository.LanguageRepository;
import de.fopro.ctest.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DatabaseSeeder {

  LanguageRepository languageRepository;
  UserRepository userRepository;
  @Autowired PasswordEncoder encoder;
  @Value("${ctestevaluator.app.administratorUser}")
  private String administratorUser;
  @Value("${ctestevaluator.app.administratorPassword}")
  private String administratorPassword;

  @Autowired
  public DatabaseSeeder(LanguageRepository languageRepository, UserRepository userRepository) {
    this.languageRepository = languageRepository;
    this.userRepository = userRepository;
  }

  @EventListener
  public void seed(ContextRefreshedEvent event) {
    seedLanguageTable();
    seedAdministrator();
  }

  private void seedLanguageTable() {
    List<Language> rs = languageRepository.findAll();
    if (rs == null || rs.size() <= 0) {
      Language german = new Language("Deutsch", "DE");
      Language english = new Language("Englisch", "EN");
      Language spanish = new Language("Spanisch", "ES");
      Language italian = new Language("Italienisch", "IT");
      languageRepository.save(german);
      languageRepository.save(english);
      languageRepository.save(spanish);
      languageRepository.save(italian);
    } else {
      System.out.println("Language Seeding Not Required");
    }
  }

  private void seedAdministrator() {
    // Create new user's account
    if (!userRepository.existsByUsername(administratorUser)) {
      User user = new User(administratorUser, encoder.encode(administratorPassword));
      userRepository.save(user);
    }
  }
}
