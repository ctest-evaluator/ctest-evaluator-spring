# C-Test Evaluator (Server)

This program is part of the C-Test Evaluator, which was developed within the scope of a research project at the University of Duisburg-Essen. The tool can be used to evaluate difficulties for a C-test or to determine the language proficiency level of a user. This application provides the server functionalities / REST API. 

- A live demo of the project can be found here: http://161.35.20.204/
- Access to the administration area can be gained using the following credentials 
    - ```login: appuser```
    - ```password: fopro123``` 

The corresponding repository of the web interface can be found [here](https://gitlab.com/ctest-evaluator/ctest-evaluator-app).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
The following items should be installed in your system:
- Java 14+
	- e.g. https://adoptopenjdk.net/
- Maven 3+
	- https://maven.apache.org/download.cgi
- MySQL 8+ (In theory other databases can be used, but the program was built with MySQL and thus we will only refer to this in the following.)
	- https://www.mysql.com/de/downloads/

### Installing
- Clone this repository 
    - ```git clone https://gitlab.com/ctest-evaluator/ctest-evaluator-spring.git```
- Please make appropriate changes in the provided properties file under ```src/main/resources```. The best way to do this is to make a copy of the file with the name application.properties and replace the given placeholders with the correct values.
    - Ensure a database schema is set up and reachable for the application with ```<DB_NAME>```, ```<DB_USER>``` and ```<DB_PASSWORD>```. The schema should necessarily be created with UTF-8 encoding, otherwise encoding errors may occur.
- Use your IDE for building and starting the application or go into the directory and build the project with
    - ```mvn clean install```
    - ```mvn spring-boot:run``` 
- On the first start the database will be seeded with all supported languages. 
- Also an administrator account for the webservice will be created with the specified values from properties. 
    - Although an endpoint is theoretically available under SERVER:PORT/api/auth/signup that can be used to add an administrator at a later stage, but this is deactivated by default. 

### Running the web application
To be able to use the application via an interface, we provide another web application. Please reference the documentation on the [C-Test Evaluator (Web Interface)](https://gitlab.com/ctest-evaluator/ctest-evaluator-app).

## Deployment

The application can be deployed and provided just like a regular Spring Boot application: 
- [Deploying Spring Boot Applications](https://docs.spring.io/spring-boot/docs/current/reference/html/deployment.html)

Nevertheless, a short side note: For the demo server, the project was built using Maven and made available through a [Tomcat](https://tomcat.apache.org/download-90.cgi) server. This includes the steps:
- bulding the project with ```mvn clean package```.
- deploying WAR file on the associated Tomcat server.


## Built With

* [Spring Boot](https://spring.io/projects/spring-boot)
* [Maven](https://maven.apache.org/) - Dependency Management